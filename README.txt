=======================================
README - Programa de Manejo de Señales
=======================================

1. DESCRIPCIÓN:
---------------

El programa de manejo de señales es un ejemplo que demuestra cómo crear procesos hijos en respuesta a señales específicas y cómo ejecutar programas del sistema desde un hijo.
Este programa utiliza las señales SIGUSR1, SIGUSR2 y SIGTERM.

2. REQUISITOS DEL SISTEMA:
---------------------------

- Un compilador de C (por ejemplo, GCC) instalado en su sistema.

3. COMPILACIÓN:
---------------

Para compilar el programa, ejecute el siguiente comando en su terminal:
gcc -o 6 ejercicio6.c

Esto generará un archivo ejecutable llamado "6".

4. USO:
------

Para ejecutar el programa, utilice el siguiente comando:
./6


El programa comenzará a ejecutarse y responderá a las señales SIGUSR1, SIGUSR2 y SIGTERM.

5. EJEMPLOS:
------------

Ejemplo 1: Enviar la señal SIGUSR1 al programa para crear un proceso hijo y ver su salida:

kill -SIGUSR1 <PID_del_programa>
kill -SIGUSR2 <PID_del_programa>
kill -SIGTERM <PID_del_programa>




